from sklearn.cluster import KMeans
import pandas as pd
def kmeans_dataset(n_clusters_list, strategies, tries):
    def run():
        for n_clusters in n_clusters_list:
            for strategy in strategies:
                for rs in tries:  # On utilisera `rs` pour fixer le `random_state`
                    km = KMeans(n_clusters=n_clusters, init=strategy, random_state=rs)
                    inertia = km.inertia_
                    yield rs, strategy, n_clusters, inertia

    return run


gen = kmeans_dataset([2, 3, 4], ["k-means++", "random"], range(10))

df = pd.DataFrame(gen(), columns=["seed", "init", "n_clusters", "inertia"])
df = df.astype({
    "seed": "int32",
    "n_clusters": "int32"
})
