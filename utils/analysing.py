# -*- coding: utf-8 -*-
"""
Linear model tuning code for Machine Learning Competition.
"""

#imports
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix


# TODO: Change the function so that the colors represent the percentage and not the absolute amounts
def plot_confusion_matrix(y_true, y_pred, figsize=(4,4)):
    """
    Generate matrix plot of confusion matrix with pretty annotations.
    args:
      y_true:    true label of the data, with shape (nsamples,)
      y_pred:    prediction of the data, with shape (nsamples,)
      labels:    string array, name the order of class labels in the confusion matrix.
                 use `clf.classes_` if using scikit-learn models.
                 with shape (nclass,).
      figsize:   the size of the figure plotted.
    """
    print(y_true)
    print(y_pred)
    cm = confusion_matrix(y_true, y_pred)
    cm_sum = np.sum(cm, axis=1, keepdims=True)
    cm_perc = cm / cm_sum.astype(float) * 100
    annot = np.empty_like(cm).astype(str)
    nrows, ncols = cm.shape
    for i in range(nrows):
        for j in range(ncols):
            c = cm[i, j]
            p = cm_perc[i, j]
            if i == j:
                s = cm_sum[i]
                annot[i, j] = '%.1f%%\n%d/%d' % (p, c, s)
            elif c == 0:
                annot[i, j] = ''
            else:
                annot[i, j] = '%.1f%%\n%d' % (p, c)
    labels = [0,1]
    cm = pd.DataFrame(cm, index=labels, columns=labels)
    cm.index.name = 'Actual'
    cm.columns.name = 'Predicted'
    fig, ax = plt.subplots(figsize=figsize)
    sns.heatmap(cm, annot=annot, fmt='', ax=ax)
    plt.show()


def most_certain_correct_predictions(test_probas, test_labels, n):
    """
    INPUT:
        test_probas: All the probabilities of all the tested samples
        n: The amount of results wanted
    OUTPUT:
        A list of tuples (indexes of the samples,probability,class)
    """
    highest_prediction_samples = []
    result = []

    # Find the samples with the highest probability
    for i in test_probas:
        highest_prediction_samples.append(max(i))

    # Sort the indices on the highest probability
    indices = sorted(range(len(highest_prediction_samples)), key=lambda i: highest_prediction_samples[i])[::-1]

    i = 0
    # Check if the prediction was correct:
    while len(result) < n:
        sample_index = indices[i]

        result_probability = highest_prediction_samples[sample_index]
        result_class = np.where(test_probas[sample_index] == result_probability)

        if int(test_labels[sample_index]) == int(result_class[0][0]):
            print("Correct prediction for index " + str(sample_index) + ", which is class " + str(
                test_labels[sample_index]))
            result.append((sample_index, result_probability, result_class[0][0]))
        else:
            print("Wrong prediction: predicted " + str(result_class[0][0]) + " but is: " + str(
                test_labels[sample_index]))
        i += 1
    return result


def most_certain_wrong_predictions(test_probas, test_labels, n):
    """
    INPUT:
        test_probas: All the probabilities of all the tested samples
        n: The amount of results wanted
    OUTPUT:
        A list of tuples (indexes of the samples,probability,class)
    """
    highest_prediction_samples = []
    result = []

    # Find the samples with the highest probability
    for i in test_probas:
        highest_prediction_samples.append(max(i))

    # Sort the indices on the highest probability
    indices = sorted(range(len(highest_prediction_samples)), key=lambda i: highest_prediction_samples[i])[::-1]

    i = 0
    # Check if the prediction was correct:
    while len(result) < n:
        sample_index = indices[i]

        result_probability = highest_prediction_samples[sample_index]
        result_class = np.where(test_probas[sample_index] == result_probability)

        if int(test_labels[sample_index]) == int(result_class[0][0]):
            print("Correct prediction for index " + str(sample_index) + ", which is class " + str(
                test_labels[sample_index]))
        else:
            print("Wrong prediction: predicted " + str(result_class[0][0]) + " but is: " + str(
                test_labels[sample_index]))
            result.append((sample_index, result_probability, result_class[0][0]))

        i += 1
    return result


def most_uncertain_predictions(test_probas, n):
    """
    INPUT:
        test_probas: All the probabilities of all the tested samples
        n: The amount of results wanted
    OUTPUT:
        A list of tuples (indexes of the samples,probability,class)
    """
    lowest_prediction_samples = []
    result = []

    # Find the samples with the highest probability
    for i in test_probas:
        lowest_prediction_samples.append(max(i))

    # Find the indices of the top n
    indices = sorted(range(len(lowest_prediction_samples)), key=lambda i: lowest_prediction_samples[i])[:n]

    # Construct the result
    for i in indices:
        result_probability = lowest_prediction_samples[i]
        result_class = np.where(test_probas[i] == result_probability)
        # result_class = test_probas[i].index(result_probability)
        result.append((i, result_probability, result_class))

    return result