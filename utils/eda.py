import pandas as pd
import numpy as np
import scipy.linalg as linalg
from sklearn.preprocessing import StandardScaler, MinMaxScaler

def read_data():
    data = pd.read_csv("./data/adult.data", header=None, delimiter=r",\s+", engine='python')
    data.columns = [
        "Age", "WorkClass", "fnlwgt", "Education", "EducationNum",
        "MaritalStatus", "Occupation", "Relationship", "Race", "Gender",
        "CapitalGain", "CapitalLoss", "HoursPerWeek", "NativeCountry", "Income"
    ]
    return data

#--------------------------Age-------------------------------------------------------------------------
## 1. reclassify
def Age_classify(data):
    def sep_age(age):
        if age<=30:
            return '1'
        elif 30<age<=50:
            return '2'
        elif 50<age<=70:
            return '3'
        elif 70<age<=90:
            return '4'

    data['Age'] = data['Age'].apply(sep_age)
    return data
#------------------------------------------------------------------------------------------------------


#--------------------------HoursPerWeek----------------------------------------------------------------
## 1. reclassify
def HoursPerWeek_classify(data):
    def sep_hour(hour):
        if hour<40:
            return '1'
        elif hour==40:
            return '2'
        else:
            return '3'

    data['HoursPerWeek'] = data['HoursPerWeek'].apply(sep_hour)
    return data
#------------------------------------------------------------------------------------------------------


#--------------------------NativeCountry---------------------------------------------------------------
## 1. replace abnormal values by mode (United-States)
def NativeCountry_replace(data):
    var = data['NativeCountry'].mode()
    data['NativeCountry'] = data['NativeCountry'].replace('?',var[0])
    return data

## 2. re-classify: 
### 'United-States' => '1', 
### else => '0'
def NativeCountry_classify(data):
    data['NativeCountry'] = pd.Series([1 if item == 'United-States' else 0 for item in data["NativeCountry"]])
    return data
#------------------------------------------------------------------------------------------------------


#--------------------------WorkClass-------------------------------------------------------------------
## 1. replace abnormal values by mode (Private), regroup three work without income
def WorkClass_replace(data):
    data['WorkClass'] = data['WorkClass'].map({ 
        "Private": "Private", 
        "?": "Private", 

        "Self-emp-not-inc": 'Not-inc',
        "Without-pay": 'Not-inc',
        "Never-worked": 'Not-inc',

        "State-gov": 'State-gov',
        "Federal-gov": 'Federal-gov',
        "Local-gov": 'Local-gov',
        "Self-emp-inc": 'Self-emp-inc'
    })
    return data
#------------------------------------------------------------------------------------------------------


#--------------------------Occupation------------------------------------------------------------------
## 1. replace abnormal values by creating a new class
def Occupation_replace(data):
    data['Occupation'] = pd.Series(['Unknown' if item == '?' else item for item in data["Occupation"]])
    return data
#------------------------------------------------------------------------------------------------------


#--------------------------EducationNum & Education----------------------------------------------------
## 1. remove 'Education'
def Education_remove(data):
    return data.drop(['Education'], axis=1)

## 2. re-classify 'EducationNum'
def EducationNum_classify(data):
    def sep_education(num):
        if num<9:
            return '1'
        elif 9<=num<10:
            return '2'
        elif 10<=num<13:
            return '3'
        else:
            return '4'

    data['EducationNum'] = data['EducationNum'].apply(sep_education)
    return data
#------------------------------------------------------------------------------------------------------


#--------------------------CapitalGain & CapitalLoss---------------------------------------------------
## 1. add a new column 'CapitalChange' = 'CapitalGain' - 'CapitalLoss'
def Capital_merge(data):
    data['CapitalChange'] = data['CapitalGain'] - data['CapitalLoss']
    return data.drop(['CapitalGain', 'CapitalLoss'], axis=1)

## 2. merge tow columns and re-classify values
def Capital_classify(data):
    def sep_capital(capital):
        if capital<0:
            return '1'
        elif capital==0:
            return '2'
        elif 0<capital<=50000:
            return '3'
        elif 50000<capital:
            return '4'
    data['CapitalChange'] = (data['CapitalGain'] - data['CapitalLoss']).apply(sep_capital)
    return data.drop(['CapitalGain', 'CapitalLoss'], axis=1)
#------------------------------------------------------------------------------------------------------

#------------------------------MaritalStatus & Relationship & Gender-----------------------------------
## 1. conclude the info of these three columns into three columns (married ? 1:0, haveChild ? 1:0, gender ? 1:0)
def Status_conclude(data):
    data['MaritalStatus'] = data["MaritalStatus"].map({ 
        "Divorced": 0, 
        "Married-AF-spouse": 1,
        "Married-civ-spouse": 1,
        "Married-spouse-absent": 0,
        "Never-married": 0,
        "Separated": 0,
        "Widowed": 0
    })
    data['HasChild'] = pd.Series([1 if item == 'Own-child' else 0 for item in data["Relationship"]])
    return data.drop(['Relationship'], axis=1)
#------------------------------------------------------------------------------------------------------


#------------------------------Get X y weight----------------------------------------------------------
def get_X_y(data, is_train=True):
    y = data.Income.map({"<=50K": 0, ">50K": 1})
    if is_train == False:
        y = data.Income.map({"<=50K.": 0, ">50K.": 1})
    #y = data["Income"].map({"<=50K": 0, ">50K": 1})
    weight = data['fnlwgt']
    X = data.drop(['fnlwgt', 'Income'], axis=1)
    return X, y, weight
#------------------------------------------------------------------------------------------------------


#------------------------------Dummy-------------------------------------------------------------------
## according to your need ! (you can print the dataframe fistly for checking)
def X_to_dummy(X, cols):
    return pd.get_dummies(X, columns=cols)
#------------------------------------------------------------------------------------------------------


#------------------------------standardization---------------------------------------------------------
## 1. Zero-centered
def X_centered(X):
    X = X.to_numpy()
    return X - X.mean(axis=0)

## 2. Normalisation
def normalisation(X):
    X = X.to_numpy()
    transfer = MinMaxScaler(feature_range=(0,1))
    return transfer.fit_transform(X)

## 3. Standardization
def standardization(X):
    #X = X.to_numpy()
    transfer = StandardScaler()
    return transfer.fit_transform(X)
#------------------------------------------------------------------------------------------------------


#------------------------------PCA---------------------------------------------------------------------
def PCA_with_weight(X, weight):
    sumWgt = float(np.sum(weight))
    Dp = np.diag(weight/sumWgt)
    print(len(Dp))
    print(len(Dp[0]))
    print(len(X))
    print(len(X[0]))


    V = np.matmul(np.matmul(np.transpose(X), Dp), X)

    val_p, vect_p = linalg.eigh(V)
    sorted_indices = np.argsort(-val_p)
    val_p = val_p[sorted_indices]
    vect_p = vect_p[:,sorted_indices]

    U = vect_p
    C = np.matmul(X, U) #transfomed X
    sumVal = np.sum(val_p)
    inertie_pourcentage = val_p / sumVal
    return C, inertie_pourcentage

#------------------------------------------------------------------------------------------------------