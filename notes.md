# SY09 UCI-Adult-dataset Analysis and ML
2020/4/17
Variables qui va traiter maintenant:  
	1.On enlève éducation car éducation et éducationNum sont les même données  
	2.WorkClass,Occupation avec ? utiliser  
		a.0  
		b.mode  
	3.Les var qualitatif  
		1.One-hot pour tous les var qualitatives  
		2.donner chaque modalité un entier  
	4.CapitalGain-Loss  
Variables qui peuvent traiter après:  
	1.Relationship,MaritalStatus,Gender sont très corrélés, on peut  
		a. on laisse seulement une var  
	2.Government peuvent combiner par example: tous les personnnes de government, tous les personnes qui n'ont pas de travail  
	3.Age:transformer à qualitatif,par ex:-18,18-25  